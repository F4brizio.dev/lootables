package com.songoda.lootables;

import com.songoda.lootables.loot.Loot;

public interface Modify {

    Loot Modify(Loot loot);
}
